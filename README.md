# Empado

A free and open-source emotion and need diary that helps you find strategies to solve everyday problems.

## Join the conversation

If you want to help out or even just ask a question, you can reach the community at https://matrix.to/#/#empado:matrix.org .

## Donate
Support the project via **[Liberapay](https://liberapay.com/Empado/)**. Every bit helps. The more money is donated, the more time I can afford to work on this project.

## Contribute

You can help through:

- **Programming.** Just take an [issue](https://gitlab.com/enjoyingfoss/empado/-/issues) and assign it to yourself. If you want to work on a feature that doesn't have an associated issue yet, report a new issue first.
- **Translating.** Go to [Weblate](https://hosted.weblate.org/projects/empado/) and choose or add your language.

## Install

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get Empado on Google Play"
      height="108">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.empado)