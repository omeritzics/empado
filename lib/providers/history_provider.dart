// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:empado/models/list_situation.dart';
import 'package:empado/providers/db_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class _FullSituationHistoryStateNotifier
    extends AsyncNotifier<List<ListSituation>> {
  @override
  FutureOr<List<ListSituation>> build() async {
    return ref
        .read(dbProvider)
        .queryAllFullSituations(); //TODO maybe do pagination so I don't have to load all these into memory
  }

  Future<void> deleteSituation(int situationId) async {
    state = const AsyncValue.loading();
    await ref.read(dbProvider).deleteSituation(situationId);
    state = AsyncValue.data(await ref
        .read(dbProvider)
        .queryAllFullSituations()); //TODO how about just copying the list without it, without asking the database again?
  }
}

final fullSituationHistoryProvider = AsyncNotifierProvider<
    _FullSituationHistoryStateNotifier,
    List<ListSituation>>(_FullSituationHistoryStateNotifier.new);
