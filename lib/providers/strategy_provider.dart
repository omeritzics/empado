// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:empado/db/database.dart';
import 'package:empado/models/list_strategy.dart';
import 'package:empado/providers/db_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StrategyState {
  final List<ListStrategy> bookmarkedNotArchived;
  final List<ListStrategy> standardNotArchived;
  final List<ListStrategy> archived;

  StrategyState(
      {required this.bookmarkedNotArchived,
      required this.standardNotArchived,
      required this.archived});
}

class _StrategyNotifier extends ChangeNotifier {
  AsyncValue<StrategyState> asyncState = const AsyncValue.loading();

  Future<void> initState(ChangeNotifierProviderRef ref) async {
    try {
      final strategies = await ref
          .read(dbProvider)
          .queryAllStrategiesByLastUsed()
          .onError((error, stackTrace) {
        throw AsyncValue<StrategyState>.error(error ?? Error(), stackTrace);
      });
      asyncState = AsyncValue.data(StrategyState(
          archived:
              strategies.where((element) => element.strategy.archived).toList(),
          bookmarkedNotArchived: strategies
              .where((element) =>
                  !element.strategy.archived && element.strategy.bookmarked)
              .toList(),
          standardNotArchived: strategies
              .where((element) =>
                  !element.strategy.archived && !element.strategy.bookmarked)
              .toList()));
    } catch (e) {
      if (e is AsyncValue<StrategyState>) {
        asyncState = e;
      } else {
        asyncState = AsyncValue.error(e, StackTrace.current);
      }
    }
    notifyListeners();
  }

  Future<void> toggleArchive(
      WidgetRef ref, StrategyState state, ListStrategy listStrategy) async {
    await ref.read(dbProvider).toggleStrategyArchive(
        listStrategy.strategy, !listStrategy.strategy.archived);
    final newStrategy = listStrategy.strategy
        .copyWith(archived: !listStrategy.strategy.archived);
    final newListStrategy = listStrategy.copyWith(strategy: newStrategy);

    if (newStrategy.archived) {
      _moveBetweenLists(
        listStrategy,
        newListStrategy,
        newStrategy.bookmarked
            ? state.bookmarkedNotArchived
            : state.standardNotArchived,
        state.archived,
      );
    } else {
      _moveBetweenLists(
        listStrategy,
        newListStrategy,
        state.archived,
        newStrategy.bookmarked
            ? state.bookmarkedNotArchived
            : state.standardNotArchived,
      );
    }
    notifyListeners();
  }

  Future<void> toggleBookmark(
      WidgetRef ref, StrategyState state, ListStrategy listStrategy) async {
    await ref.read(dbProvider).toggleStrategyBookmark(
        listStrategy.strategy, !listStrategy.strategy.bookmarked);
    final newStrategy = listStrategy.strategy
        .copyWith(bookmarked: !listStrategy.strategy.bookmarked);
    final newListStrategy = listStrategy.copyWith(strategy: newStrategy);

    if (newStrategy.archived) {
      _moveBetweenLists(
        listStrategy,
        newListStrategy,
        state.archived,
        state.archived,
      );
    } else {
      _moveBetweenLists(
        listStrategy,
        newListStrategy,
        newStrategy.bookmarked
            ? state.standardNotArchived
            : state.bookmarkedNotArchived,
        newStrategy.bookmarked
            ? state.bookmarkedNotArchived
            : state.standardNotArchived,
      );
    }

    notifyListeners();
  }

  static void _moveBetweenLists(ListStrategy oldItem, ListStrategy newItem,
      List<ListStrategy> from, List<ListStrategy> to) {
    from.remove(oldItem);
    to
      ..add(newItem)
      ..sort(((a, b) => b.strategy.lastUsed.compareTo(a.strategy.lastUsed)));
  }
}

final strategyProvider = ChangeNotifierProvider<_StrategyNotifier>((ref) {
  final notifier = _StrategyNotifier();
  unawaited(notifier.initState(ref));
  return notifier;
});
