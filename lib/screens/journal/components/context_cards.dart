// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/models/temporary_person.dart';
import 'package:empado/providers/situation_provider.dart';
import 'package:empado/screens/journal/components/context_card.dart';
import 'package:empado/screens/journal/components/situation_sheet/situation_sheet.dart';
import 'package:empado/screens/journal/context_dialogs/note_dialog.dart';
import 'package:empado/screens/journal/context_dialogs/observation_dialog.dart';
import 'package:empado/screens/journal/context_dialogs/strategy_dialog.dart';
import 'package:empado/utils/string_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ContextCards extends ConsumerWidget {
  final TemporaryPerson person;

  const ContextCards({required this.person, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final perspective =
        ref.watch(situationProvider).state.perspectives[person]!;
    final perspectiveNotifier = ref.read(situationProvider.notifier);

    final viewPadding = MediaQuery.of(context).viewPadding;

    return ListView(
      padding: EdgeInsets.fromLTRB(
          viewPadding.left, 8, viewPadding.right, SituationSheet.height),
      children: [
        if (!person.isYou())
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: SwitchListTile.adaptive(
                title: Text(
                    AppLocalizations.of(context).describedOwnPerspective(
                        person.name!, person.pronoun.localeKey),
                    style: Theme.of(context).textTheme.bodyMedium),
                value: perspective.ownDescription,
                onChanged: (value) =>
                    perspectiveNotifier.setOwnDescription(person, value),
              )),
        ContextCard(
          title: AppLocalizations.of(context).relevantObservation,
          subtitle: AppLocalizations.of(context).relevantObservationDesc,
          content: perspective.observation,
          assetPath: "assets/context_observation.svg",
          fullScreenDialog: ObservationDialog(person: person),
          onDelete: () {
            perspectiveNotifier.setObservation(person, null);
          },
        ),
        ContextCard(
            title: (perspective.isAppreciation
                ? AppLocalizations.of(context).strategiesUsed
                : AppLocalizations.of(context).potentialStrategies),
            subtitle: (perspective.isAppreciation
                ? AppLocalizations.of(context).strategiesUsedDesc
                : AppLocalizations.of(context).potentialStrategiesDesc),
            content: perspective.strategies.isNotEmpty
                ? StringUtil.toBulletedList(
                    perspective.strategies.map((s) => s.strategy.value))
                : null,
            assetPath: "assets/context_strategies.svg",
            fullScreenDialog: StrategyDialog(person: person),
            onDelete: () {
              perspectiveNotifier.setStrategies(person, null);
            }),
        ContextCard(
            title: AppLocalizations.of(context).additionalNotes,
            subtitle: AppLocalizations.of(context).additionalNotesDesc,
            content: perspective.additionalNotes,
            assetPath: "assets/context_notes.svg",
            fullScreenDialog: NoteDialog(person: person),
            onDelete: () {
              perspectiveNotifier.setNotes(person, null);
            }),
        Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 16),
            child: Text(
              AppLocalizations.of(context).contextInfoOptional,
              style: Theme.of(context).textTheme.bodySmall?.copyWith(
                  color: Theme.of(context)
                      .colorScheme
                      .onBackground
                      .withAlpha(191)),
              textAlign: TextAlign.center,
            )),
      ],
    );
  }
}
