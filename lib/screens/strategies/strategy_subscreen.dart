// Copyright Miroslav Mazel
//
// This file is part of Empado.
//
// Empado is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// As an additional permission under section 7, you are allowed to distribute
// the software through an app store, even if that store has restrictive terms
// and conditions that are incompatible with the AGPL, provided that the source
// is also available under the AGPL with or without this permission through a
// channel without those restrictive terms and conditions.
//
// As a limitation under section 7, all unofficial builds and forks of the app
// must be clearly labeled as unofficial in the app's name (e.g. "Empado
// UNOFFICIAL", never just "Empado") or use a different name altogether.
// If any code changes are made, the fork should use a completely different name
// and app icon. All unofficial builds and forks MUST use a different
// application ID, in order to not conflict with a potential official release.
//
// Empado is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Empado.  If not, see <http://www.gnu.org/licenses/>.

import 'package:empado/components/error_content.dart';
import 'package:empado/models/list_strategy.dart';
import 'package:empado/providers/strategy_provider.dart';
import 'package:empado/screens/strategies/components/strategy_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StrategySubscreen extends ConsumerWidget {
  const StrategySubscreen({super.key});

  //TODO how to deal with strategies for others?

  //TODO add empty state that explains how strategies are added

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final asyncStrategies = ref.watch(strategyProvider).asyncState;
    final asyncStrategyNotifier = ref.read(strategyProvider.notifier);
    //TODO allow exports
    return asyncStrategies.when(
        data: (listStrategies) => CustomScrollView(slivers: [
              SliverAppBar(
                  title: Text(AppLocalizations.of(context).strategies)),
              if (listStrategies.bookmarkedNotArchived.isNotEmpty)
                _SectionHeader(text: AppLocalizations.of(context).bookmarked),
              SliverList.builder(
                  itemCount: listStrategies.bookmarkedNotArchived.length,
                  itemBuilder: (_, i) => _StrategyListTile(
                      strategyState: listStrategies,
                      listStrategy: listStrategies.bookmarkedNotArchived[i],
                      onToggleArchive: asyncStrategyNotifier.toggleArchive,
                      onToggleBookmark: asyncStrategyNotifier.toggleBookmark)),
              if (listStrategies.standardNotArchived.isNotEmpty &&
                  listStrategies.bookmarkedNotArchived.isNotEmpty)
                _SectionHeader(
                    text: AppLocalizations.of(context).miscStrategies),
              SliverList.builder(
                  itemCount: listStrategies.standardNotArchived.length,
                  itemBuilder: (_, i) => _StrategyListTile(
                      strategyState: listStrategies,
                      listStrategy: listStrategies.standardNotArchived[i],
                      onToggleArchive: asyncStrategyNotifier.toggleArchive,
                      onToggleBookmark: asyncStrategyNotifier.toggleBookmark)),
              if (listStrategies.archived.isNotEmpty)
                _SectionHeader(text: AppLocalizations.of(context).archived),
              SliverList.builder(
                  itemCount: listStrategies.archived.length,
                  itemBuilder: (_, i) => _StrategyListTile(
                        strategyState: listStrategies,
                        listStrategy: listStrategies.archived[i],
                        onToggleArchive: asyncStrategyNotifier.toggleArchive,
                      ))
            ]),
        error: (_, stackTrace) => const Center(child: ErrorContent()),
        loading: () =>
            const Center(child: CircularProgressIndicator.adaptive()));
    // TODO allow deleting items
  }
}

class _SectionHeader extends StatelessWidget {
  final String text;
  const _SectionHeader({required this.text});

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
        child: Padding(
      padding: const EdgeInsets.fromLTRB(16, 24, 16, 0),
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .labelMedium
            ?.copyWith(color: Theme.of(context).colorScheme.primary),
      ),
    ));
  }
}

class _StrategyListTile extends ConsumerWidget {
  //TODO allow filtering by need category / by need

  //TODO should not include strategies for others!

  final Future Function(WidgetRef, StrategyState, ListStrategy) onToggleArchive;
  final Future Function(WidgetRef, StrategyState, ListStrategy)?
      onToggleBookmark;
  final StrategyState strategyState;
  final ListStrategy listStrategy;

  const _StrategyListTile(
      {required this.strategyState,
      required this.listStrategy,
      required this.onToggleArchive,
      this.onToggleBookmark});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final strategy = listStrategy.strategy;
    return ListTile(
      title: Text(strategy.strategy),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            tooltip: strategy.archived
                ? AppLocalizations.of(context).unarchive
                : AppLocalizations.of(context).archive,
            icon: Icon(strategy.archived ? Icons.unarchive : Icons.archive),
            onPressed: () async =>
                await onToggleArchive(ref, strategyState, listStrategy),
          ),
          if (onToggleBookmark != null)
            IconButton(
                tooltip: strategy.bookmarked
                    ? AppLocalizations.of(context).removeBookmark
                    : AppLocalizations.of(context).bookmark,
                icon: Icon(strategy.bookmarked
                    ? Icons.bookmark
                    : Icons.bookmark_border),
                onPressed: () async =>
                    await onToggleBookmark!(ref, strategyState, listStrategy))
        ], //TODO share on swipe; on Windows and Linux, just copy to clipboard
        // TODO add a tip about using a todo and/or habit app. Perhaps add an info icon to the appbar?
      ),
      onTap: () async =>
          await StrategySheet.showSheet(context, listStrategy: listStrategy),
    );
  }
}
